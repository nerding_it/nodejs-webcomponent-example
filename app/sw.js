const channel = new BroadcastChannel('sw');

const DEFAULT_PAGE_SIZE = 5;

/* State of the application */
const cache = {
    data: null,
    currentPage: null,
    searchQuery: null,
    totalPages: null,
    pageSize: DEFAULT_PAGE_SIZE
}

/* Retrieve data using search string */
async function retrieveResults() {
    const url = `${self.location.origin}/api?search=${cache.searchQuery}`;
    console.debug(`Retrieving from url ${url}`);
    const response = await fetch(url);
    return await response.json();
}

function performPagination () {
    const startIndex = cache.currentPage * cache.pageSize;
    const endIndex = startIndex + cache.pageSize;
    const data = cache.data.slice(startIndex, endIndex);
    console.debug(`Performing pagination from element ${startIndex} to ${endIndex}`);
    channel.postMessage({type: 'SEARCH_RESULT', data: data});
}

self.addEventListener('install', function (event) {
    console.debug(event);
});

channel.addEventListener('message', async event => {
    switch (event.data.type) {
	case 'SEARCH':
	    cache.searchQuery = event.data.payload.search;
	    console.debug(`Searching for ${cache.searchQuery}`);
	    const result = await retrieveResults();
	    console.assert(Array.isArray(result.data), "Response should be an array");
	    cache.data = result.data;
	    cache.totalPages = Math.ceil(cache.data.length / cache.pageSize);
	    cache.currentPage = 0;
	    performPagination();
	    break;
	case 'GET_LAST_RESULT':
	    if (cache.searchQuery) {
		const result = await retrieveResults();
		console.assert(Array.isArray(result.data), "Response should be an array");
		cache.data = result.data;
		cache.totalPages = Math.ceil(cache.data.length / cache.pageSize);
		cache.currentPage = 0;
		channel.postMessage({type: 'RECENT_SEARCH', data: cache.searchQuery});
		performPagination();
	    }
	    break;
	case 'PAGINATION_NEXT':
	    console.assert(cache.currentPage > cache.totalCount, 'Something is wrong with pagination');
	    if (cache.currentPage < cache.totalPages - 1) {
		cache.currentPage += 1;
		performPagination();		
	    }
	    break;
	case 'PAGINATION_PREV':
	    if (cache.currentPage > 0) {
		cache.currentPage -= 1;
		performPagination();
	    }
	    break;
	case 'SORT_BY_DATE':	    
	    cache.data = cache.data.sort((first, second) => {
		return new Date(first.dateLastEdited) < new Date(second.dateLastEdited);
	    });
	    cache.currentPage = 0;
	    performPagination();
	    break;
	case 'SORT_BY_TITLE':	    
	    cache.data = cache.data.sort((first, second) => {
		return first.name.trim().replace(/\s/, '') < second.name.trim().replace(/\s/, '');
	    });
	    cache.currentPage = 0;
	    performPagination();
	    break;	    
	default:
	    console.error(`Unknown message type ${event.data.type}`);
	    break;
    }
});
