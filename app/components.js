const channel = new BroadcastChannel('sw');

class SearchBox extends HTMLElement {
    
    constructor() {
	super();		 
	const shadow = this.attachShadow({mode: 'open'});
	shadow.innerHTML = `<form name="search">
	    <input name="search" type="text" />
	    <button type="submit"> Apply </button>
	</form>`;
    }
    
    performSearch = (event) => {
	event.preventDefault();
	const form = {};
	for (const input of event.target.querySelectorAll('input')) {
	    form[input.name] = input.value.trim();
	}
	channel.postMessage({type: 'SEARCH', payload: form});
    }

    onmessage = (event) => {
	switch (event.data.type) {
	    case 'RECENT_SEARCH':
		this.shadowRoot.querySelector('form[name="search"]').querySelector('input[name="search"]').value= event.data.data;
		break;
	    default:
		break;
	}		 
    }
    
    connectedCallback (event) {
	this.shadowRoot.querySelector('form[name="search"]').addEventListener('submit', this.performSearch);
	channel.addEventListener('message', this.onmessage);
    }
    
    disconnectedCallback = (event) => {
	channel.removeEventListener('message', this.onmessage);
    }
}

class FilterPagination extends HTMLElement {
    constructor() {
	super();
	this.attachShadow({mode: 'open'});
    }
    connectedCallback() {
	const nextBtn = document.createElement('button');
	nextBtn.textContent = 'NEXT';
	const prevBtn = document.createElement('button');
	prevBtn.textContent = 'PREV';		 		 
	nextBtn.addEventListener('click', (event) => {
	    channel.postMessage({type: 'PAGINATION_NEXT'})
	})
	prevBtn.addEventListener('click', (event) => {
	    channel.postMessage({type: 'PAGINATION_PREV'})
	});
	this.shadowRoot.appendChild(prevBtn);
	this.shadowRoot.appendChild(nextBtn);
    }
}

class FilterSort extends HTMLElement {
    constructor() {
	super();
	this.attachShadow({mode: 'open'});
    }
    connectedCallback() {
	const select = document.createElement('select');
	['name', 'date'].forEach((item) => {
	    const option = document.createElement('option');
	    option.value = item;
	    option.textContent = item;
	    select.appendChild(option);
	});
	this.shadowRoot.appendChild(select);
	select.addEventListener('change', function () {
	    switch (this.value) {
		case 'date':
		    channel.postMessage({type: 'SORT_BY_DATE'});
		    break;
		case 'name':
		    channel.postMessage({type: 'SORT_BY_TITLE'});
		    break;
		default:
		    break;
	    }
	})		 
    }
}

class PaginatedTable extends HTMLElement {
    constructor() {
	super();
	this.attachShadow({mode: 'open'});
    }
    
    onmessage = (event) => {
	switch (event.data.type) {
	    case 'SEARCH_RESULT':
		this.render(event.data.data);
		break;
	    default:
		console.error(`Message type is unknown ${event.data.type}`);
		break;
	}		 
    }
    
    render(data) {
	if (this.shadowRoot.querySelector('table')) {
	    this.shadowRoot.querySelector('table').remove();
	}
	const table = document.createElement('table');
	this.shadowRoot.appendChild(table);
	const tbody = document.createElement('tbody');
	const thead = document.createElement('thead');
	table.appendChild(thead);
	table.appendChild(tbody);
	
	const tr = document.createElement('tr');
	Object.keys(data[0]).forEach((column) => {
	    const th = document.createElement('th');
	    th.textContent = column;
	    tr.append(th);
	});
	thead.appendChild(tr);
	data.forEach((row) => {
	    const tr = document.createElement('tr');
	    Object.keys(row).forEach((column) => {
		const td = document.createElement('td');
		td.textContent = row[column];
		tr.append(td);
	    })
	    tbody.appendChild(tr);
	});		 		     		 
    }
    
    connectedCallback() {
	channel.addEventListener('message', this.onmessage);
    }

    disconnectedCallback() {
	channel.removeEventListener('message', this.onmessage);
    }
}

class Grid extends HTMLElement {
    constructor() {
	super();
	this.attachShadow({mode: 'open'});
    }

    render(data) {
	if (this.shadowRoot.querySelector('div')) {
	    this.shadowRoot.querySelector('div').remove();
	}
	const div = document.createElement('div');
	this.shadowRoot.appendChild(div);
	data.forEach((item) => {
	    const img = document.createElement('img');
	    img.src = item.image;
	    div.appendChild(img);
	})
    }

    onmessage = (event) => {
	switch (event.data.type) {
	    case 'SEARCH_RESULT':
		this.render(event.data.data);
		break;
	    default:
		console.error(`Message type is unknown ${event.data.type}`);
		break;
	}		 
    }
    
    connectedCallback() {
	channel.addEventListener('message', this.onmessage);
    }

    disconnectedCallback() {
	channel.removeEventListener('message', this.onmessage);
    }    
}

customElements.define('paginated-table', PaginatedTable);
customElements.define('search-box', SearchBox);
customElements.define('filter-pagination', FilterPagination);
customElements.define('filter-sort', FilterSort);
customElements.define('display-grid', Grid);

window.addEventListener('DOMContentLoaded', function (event) {
    channel.postMessage({type: 'GET_LAST_RESULT'});
});
