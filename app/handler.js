import { parse as parser } from 'url';

let routes = [];

/* 
 * Handler for the server
 * @param {request} Request object
 * @param {response} Response object
 */
async function handler(request, response) {
    console.info('Starting processing the request');
    let route;
    const url = parser(request.url);
    for(let i = 0;i < routes.length;i++) {
	const item = routes[i];
	console.debug(`Trying to match ${url.pathname} with ${item.url}`);
	if (url.pathname.match(new RegExp(`${item.url}$`))) {
	    route = item.service;
	    console.debug(`Found route for ${item.url}`);
	    break;
	}
    }
    if (!route) {
	console.warn(`No routes found for ${url.pathname}`);
	response.writeHead(404, { 'Content-Type': 'application/json'});
	response.write(JSON.stringify({
	    'status': `${url.pathname} not exist`
	}));
    }
    else {
	const service = new route();
	const params = parser(request.url, true).query;	
	const method = request.method.toUpperCase();
	const self = {
	    query: params,
	    url: request.url,
	    headers: { 'Content-Type': 'application/json' }
	};
	if (service[method] instanceof Function) {
	    console.debug(`Found function ${service[method].name}`);
	    const result = await service[method].apply(self);
	    console.warn(self.headers);
	    response.writeHead(200, self.headers);
	    if (typeof(result) === 'string')
		response.write(result);
	    else
		response.write(JSON.stringify(result));
	}
	else {
	    console.debug(`No function found for ${url.pathname}`);
	    response.writeHead(405, { 'Content-Type': 'application/json'});
	    response.write(JSON.stringify({
		'status': `Method ${method} not allowed`
	    }));
	}	
    }
    response.end();
}

function register(url, service) {
    console.info(`Adding new route ${url}`);
    routes.push({url: url, service: service});
}

export { handler, register, routes };
