import { handler, register, routes } from './handler.js';
import { equal } from 'assert';
import  sinon from 'sinon';
import { ApiService } from './services.js';
import { SearchBox } from './components.js';

function supressLogs () {
    console.log = () => {}
    console.info = () => {}
    console.debug = () => {}
}

describe('Routing', function () {
    beforeEach(function () {
	supressLogs();
    });
    
    it('Should add new route', function () {
	const mockClass = sinon.fake();
	register('/', mockClass);
	equal(routes[0].url, '/');
	equal(routes[0].service, mockClass);
    })
})

describe('Handling', function () {
    
    const defaultService = sinon.fake();
    const response = {
	writeHead: function() {},
	write: function() {},
	end: function() {}
    };
    const request = {
	url: null,
	method: null	
    };
    var mockResponse;
    
    beforeEach(function () {
	register('/', defaultService);
	mockResponse = sinon.mock(response);
	supressLogs();
    });

    it('Should throw not found error', async function () {
	request.url = '/api';
	request.method = 'get';
	mockResponse.expects('writeHead').withArgs(404);
	await handler(request, response);
	mockResponse.verify();
    });

    it('Should throw method not allowed', async function () {
	function testService () {
	    this.GET = function() { }
	};
	const mockService = sinon.mock(testService);
	register('/api', testService);
	request.url = '/api';
	request.method = 'post';
	mockResponse.expects('writeHead').withArgs(405);
	await handler(request, response);
	mockResponse.verify();	
    });

    it('Should respond with success', async function () {
	function testService () {
	    this.GET = function() { }
	};
	const mockService = sinon.mock(testService);
	register('/api', testService);
	request.url = '/api';
	request.method = 'get';
	mockResponse.expects('writeHead').withArgs(200);
	await handler(request, response);
	mockResponse.verify();	
    });    
})

describe('Services', function () {
    describe('API service', function () {
	it ('Gets the response', async function () {
	    const service = new ApiService();
	    const result = await service.GET();
	    equal(result.status, 'success');
	})
    })
})

describe('Components', function () {
    /* TODO: Need to add component tests */
})
