import { createServer as server } from 'http';
import { handler, register } from './handler.js';
import { IndexService, SwService, ApiService, HealthService, ComponentService } from './services.js';

register('/api', ApiService);
register('/health', HealthService);
register('/', IndexService);
register('/sw.js', SwService);
register('/components.js', ComponentService);

server(handler).listen(9090, async () => {
    console.info("Listening.");
})
