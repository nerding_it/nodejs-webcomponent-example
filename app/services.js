import { promises as fs } from 'fs';
import { createHash } from 'crypto';
import { parse as parser } from 'url';

class Cache {
    constructor(size = 100) {
	this.size = size;
	this.cache = new Map();
    }
    set(key, val) {
	if (this.cache.has(key)) {
	    this.cache.delete(key);
	}
	else if (this.cache.size === this.size) {
	    this.cache.delete(this.first())
	}
	this.cache.set(key, val);
    }
    get(key) {
	const item = this.cache.get(key);
	if (item) {
	    this.cache.delete(key);
	    this.cache.set(key, item)
	}
	return item;
    }
    first() {
	return this.cache.keys().next().value;
    }
}

const storage = {
    data: null
}

const cache = new Cache();

async function loadData () {
    storage.data = await fs.readFile('data.json', 'utf8');
    storage.data = JSON.parse(storage.data);
}

loadData();

/* Service which is responsible for loading the index page */
class IndexService {    
    async GET() {
	this.headers['Content-Type'] = 'text/html';
	return await fs.readFile('index.html', 'utf8');
    }
}

/* Service responsible for dispatching the worker */
class SwService {
    async GET() {
	this.headers['Content-Type'] = 'text/javascript';
	return await fs.readFile('sw.js', 'utf8');
    }    
}

/* Service responsible for dispatching the components */
class ComponentService {
    async GET() {
	this.headers['Content-Type'] = 'text/javascript';
	return await fs.readFile('components.js', 'utf8');
    }    
}

class ApiService {
    async GET() {
	let result;
	console.info(`Search query ${this.query.search}`);
	this.query.search = this.query.search.trim();
	if (this.query.search) {
	    const hash = createHash('md5').update(this.query.search).digest('hex');
	    if (cache.get(hash)) {
		console.debug(`Using cache for search query = ${this.query.search}`);
		result = cache.get(hash);
	    }
	    else {
		if (this.query.search.match(/^\"[a-zA-Z0-9,.\s]+\"$/)) {
		    const searchQuery = this.query.search.replace(/\"/g, "");
		    result = storage.data.filter((item) => {		   
			return item.name.toLowerCase() === searchQuery.toLowerCase() ||
			       item.description.toLowerCase() === searchQuery.toLowerCase();
		    })		    
		}
		else {
		    result = storage.data.filter((item) => {
			return item.name.toLowerCase().includes(this.query.search.toLowerCase()) ||
			       item.description.toLowerCase().includes(this.query.search.toLowerCase());
		    })
		}
		cache.set(hash, result);
	    }
	}
	else {
	    result = storage.data;
	}
	return await {
	    data: result,
	    total: result.length
	}
    }
}

class HealthService {
    async GET() {
	return {'status': 'running'};
    }
}

export { IndexService, SwService, ApiService, HealthService, ComponentService };
